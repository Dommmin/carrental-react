<?php

namespace App\Enum;

enum TransmissionType: string
{
    case Manual = 'Manual';
    case Automatic = 'Automatic';
}