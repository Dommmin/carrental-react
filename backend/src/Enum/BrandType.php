<?php

namespace App\Enum;

enum BrandType: string
{
    case BMW = 'BMW';
    case MERCEDES = 'MERCEDES';
    case FORD = 'FORD';
    case VW = 'VW';
    case AUDI = 'AUDI';
    case SKODA = 'SKODA';
    case PORSCHE = 'PORSCHE';
    case TOYOTA = 'TOYOTA';
    case HONDA = 'HONDA';
    case NISSAN = 'NISSAN';
    case JAGUAR = 'JAGUAR';
    case JEEP = 'JEEP';
    case HYUNDAI = 'HYUNDAI';
    case MINI = 'MINI';
    case KIA = 'KIA';
    case CHEVROLET = 'CHEVROLET';
    case LEXUS = 'LEXUS';
    case SUBARU = 'SUBARU';
    case RENAULT = 'RENAULT';
    case SUZUKI = 'SUZUKI';
    case MITSUBISHI = 'MITSUBISHI';
    case VOLKSWAGEN = 'VOLKSWAGEN';
    case SMART = 'SMART';
    case FERRARI = 'FERRARI';
    case LAMBORGHINI = 'LAMBORGHINI';
    case MASERATI = 'MASERATI';
    case BENTLEY = 'BENTLEY';
    case BUGATTI = 'BUGATTI';
}