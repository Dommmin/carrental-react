<?php

namespace App\Enum;

enum DriveType: string
{
    case AWD = 'AWD';
    case FWD = 'FWD';
    case RWD = 'RWD';
}