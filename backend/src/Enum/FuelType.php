<?php

namespace App\Enum;

enum FuelType: string
{
    case Petrol = 'Petrol';
    case Diesel = 'Diesel';
    case Electric = 'Electric';
}
