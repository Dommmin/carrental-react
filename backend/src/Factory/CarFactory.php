<?php

namespace App\Factory;

use App\Entity\Car;
use App\Repository\CarRepository;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;
use Zenstruck\Foundry\RepositoryProxy;

/**
 * @extends ModelFactory<Car>
 *
 * @method        Car|Proxy create(array|callable $attributes = [])
 * @method static Car|Proxy createOne(array $attributes = [])
 * @method static Car|Proxy find(object|array|mixed $criteria)
 * @method static Car|Proxy findOrCreate(array $attributes)
 * @method static Car|Proxy first(string $sortedField = 'id')
 * @method static Car|Proxy last(string $sortedField = 'id')
 * @method static Car|Proxy random(array $attributes = [])
 * @method static Car|Proxy randomOrCreate(array $attributes = [])
 * @method static CarRepository|RepositoryProxy repository()
 * @method static Car[]|Proxy[] all()
 * @method static Car[]|Proxy[] createMany(int $number, array|callable $attributes = [])
 * @method static Car[]|Proxy[] createSequence(iterable|callable $sequence)
 * @method static Car[]|Proxy[] findBy(array $attributes)
 * @method static Car[]|Proxy[] randomRange(int $min, int $max, array $attributes = [])
 * @method static Car[]|Proxy[] randomSet(int $number, array $attributes = [])
 */
final class CarFactory extends ModelFactory
{
    /**
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#factories-as-services
     *
     * @todo inject services if required
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#model-factories
     *
     * @todo add your default values here
     */
    protected function getDefaults(): array
    {
        return [
            'acceleration' => self::faker()->text(),
            'available' => self::faker()->boolean(),
            'brand' => self::faker()->text(10),
            'color' => self::faker()->text(10),
            'createdAt' => self::faker()->dateTime(),
            'description' => self::faker()->text(),
            'drivetrain' => self::faker()->text(10),
            'fuel' => self::faker()->text(5),
            'fuelConsumption' => self::faker()->numberBetween(1, 10),
            'horsepower' => self::faker()->numberBetween(300, 1000),
            'model' => self::faker()->text(10),
//            'name' => self::faker()->text(20),
            'price' => self::faker()->numberBetween(100, 5000),
            'seats' => self::faker()->numberBetween(2, 9),
//            'slug' => self::faker()->text(100),
            'torque' => self::faker()->numberBetween(400, 1200),
            'transmission' => self::faker()->text(10),
            'updatedAt' => self::faker()->dateTime(),
            'year' => self::faker()->numberBetween(2020, 2023),
            'filePath' => self::faker()->imageUrl
        ];
    }

    /**
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#initialization
     */
    protected function initialize(): self
    {
        return $this
            // ->afterInstantiate(function(Car $car): void {})
        ;
    }

    protected static function getClass(): string
    {
        return Car::class;
    }
}
