<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use App\Repository\RentRepository;
use Carbon\Carbon;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Serializer\Attribute\Groups;

#[ORM\Entity(repositoryClass: RentRepository::class)]
#[ApiResource]
class Rent
{
    use TimestampableEntity;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(['rent:read', 'car:item:read'])]
    private ?int $id = null;

    #[ORM\Column]
    #[Groups(['rent:read', 'car:item:read'])]
    private \DateTimeImmutable $startDate;

    #[ORM\Column]
    #[Groups(['rent:read', 'car:item:read'])]
    private \DateTimeImmutable $endDate;

    #[ORM\ManyToOne(cascade: ['persist'], inversedBy: 'rents')]
    #[ORM\JoinColumn(nullable: false)]
    private User $user;

    #[ORM\ManyToOne(inversedBy: 'rents')]
    #[ORM\JoinColumn(nullable: false)]
    private Car $car;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStartDate(): int
    {
        return Carbon::instance($this->startDate)->getTimestamp();
    }

    public function setStartDate(\DateTimeImmutable $startDate): static
    {
        $this->startDate = Carbon::instance($startDate)->addHour()->toDateTimeImmutable();

        return $this;
    }

    public function getEndDate(): int
    {
        return Carbon::instance($this->endDate)->getTimestamp();
    }

    public function setEndDate(\DateTimeImmutable $endDate): static
    {
        $this->endDate = Carbon::instance($endDate)->addHour()->toDateTimeImmutable();

        return $this;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function setUser(User $user): static
    {
        $this->user = $user;

        return $this;
    }

    public function getCar(): Car
    {
        return $this->car;
    }

    public function setCar(Car $car): static
    {
        $this->car = $car;

        return $this;
    }
}
