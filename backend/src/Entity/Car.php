<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Post;
use App\Enum\BrandType;
use App\Enum\DriveType;
use App\Enum\FuelType;
use App\Enum\TransmissionType;
use App\Repository\CarRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

#[Vich\Uploadable]
#[ORM\Entity(repositoryClass: CarRepository::class)]
#[UniqueEntity(fields: ['brand', 'model', 'color'], message: 'There is already a car with this brand, model and color.')]
#[ApiResource(
    types: ['https://schema.org/Book'],
    operations: [
        new GetCollection(),
        new Get(
            normalizationContext: ['groups' => ['car:item:read']],
        ),
        new Post(inputFormats: ['multipart' => ['multipart/form-data']]),
        new Patch(),
        new Delete(),
    ],
    normalizationContext: ['groups' => ['car:read']],
    denormalizationContext: ['groups' => ['car:write']],
    order: ['id' => 'desc'],
    paginationItemsPerPage: 10,
)]
class Car
{
    use TimestampableEntity;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[ApiProperty(identifier: false)]
    #[Groups(['car:read', 'car:item:read'])]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[Assert\NotBlank]
    #[Groups(['car:read', 'car:write', 'car:item:read'])]
    private string $brand;

    #[ORM\Column(length: 255)]
    #[Assert\NotBlank]
    #[Groups(['car:read', 'car:write', 'car:item:read'])]
    private string $model;

    #[ORM\Column(length: 255)]
    #[Assert\NotBlank]
    #[Groups(['car:read', 'car:write', 'car:item:read'])]
    private string $color;

    #[ORM\Column(length: 255)]
    #[Groups(['car:read', 'car:write', 'car:item:read'])]
    private string $name;

    #[ORM\Column(length: 100, unique: true)]
    #[Gedmo\Slug(fields: ['name', 'color'])]
    #[ApiProperty(identifier: true)]
    #[Groups(['car:read', 'car:write', 'car:item:read'])]
    private string $slug;

    #[ORM\Column()]
    #[Assert\NotBlank]
    #[Groups(['car:read', 'car:write', 'car:item:read'])]
    private int|string $year;

    #[ORM\Column(length: 255)]
    #[Assert\NotBlank]
    #[Groups(['car:read', 'car:write', 'car:item:read'])]
    private string $fuel;

    #[ORM\Column()]
    #[Assert\NotBlank]
    #[Groups(['car:read', 'car:write', 'car:item:read'])]
    private float|string $fuelConsumption;

    #[ORM\Column()]
    #[Assert\NotBlank]
    #[Groups(['car:read', 'car:write', 'car:item:read'])]
    private int|string $seats;

    #[ORM\Column(length: 255)]
    #[Assert\NotBlank]
    #[Groups(['car:read', 'car:write', 'car:item:read'])]
    private string $transmission;

    #[ORM\Column()]
    #[Assert\NotBlank]
    #[Groups(['car:read', 'car:write', 'car:item:read'])]
    private int|string $price;

    #[ORM\Column()]
    #[Assert\NotBlank]
    #[Groups(['car:read', 'car:write', 'car:item:read'])]
    private int|string $horsepower;

    #[ORM\Column()]
    #[Assert\NotBlank]
    #[Groups(['car:read', 'car:write', 'car:item:read'])]
    private int|string $torque;

    #[ORM\Column()]
    #[Assert\NotBlank]
    #[Groups(['car:read', 'car:write', 'car:item:read'])]
    private string $acceleration;

    #[ORM\Column(nullable: true)]
    #[Groups(['car:read', 'car:write', 'car:item:read'])]
    private float|string|null $engineSize = null;

    #[ORM\Column(length: 10)]
    #[Assert\NotBlank]
    #[Groups(['car:read', 'car:write', 'car:item:read'])]
    private string $drivetrain;

    #[ORM\Column(type: Types::TEXT)]
    #[Assert\NotBlank]
    #[Assert\Length(min: 10)]
    #[Groups(['car:read', 'car:write', 'car:item:read'])]
    private string $description;

    #[ORM\Column]
    #[Groups(['car:read', 'car:write', 'car:item:read'])]
    private bool $available;

    #[ApiProperty(types: ['https://schema.org/contentUrl'])]
    public ?string $contentUrl = null;

    #[Vich\UploadableField(mapping: "media_object", fileNameProperty: "filePath")]
    #[Groups(['car:write', 'car:read'])]
    public ?File $file = null;

    #[ORM\Column(nullable: true)]
    public ?string $filePath = null;

    #[ORM\OneToMany(mappedBy: 'car', targetEntity: MediaObject::class)]
    #[ApiProperty(fetchEager: true)]
    #[Groups(['car:item:read'])]
    private Collection $images;

    #[ORM\OneToMany(mappedBy: 'car', targetEntity: Rent::class, orphanRemoval: true)]
    #[Groups(['car:item:read'])]
    private Collection $rents;

    public function __construct()
    {
        $this->images = new ArrayCollection();
        $this->rents = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): static
    {
        $this->slug = $slug;

        return $this;
    }

    public function getYear(): ?int
    {
        return $this->year;
    }

    public function setYear(int $year): static
    {
        $this->year = $year;

        return $this;
    }

    public function getFuel(): ?string
    {
        return $this->fuel;
    }

    public function setFuel(string $fuel): static
    {
        $this->fuel = $fuel;

        return $this;
    }

    public function getFuelConsumption(): int
    {
        return $this->fuelConsumption;
    }

    public function setFuelConsumption(int $fuelConsumption): static
    {
        $this->fuelConsumption = $fuelConsumption;

        return $this;
    }

    public function getSeats(): int
    {
        return $this->seats;
    }

    public function setSeats(int $seats): static
    {
        $this->seats = $seats;

        return $this;
    }

    public function getTransmission(): ?string
    {
        return $this->transmission;
    }

    public function setTransmission(string $transmission): static
    {
        $this->transmission = $transmission;

        return $this;
    }

    public function getPrice(): ?int
    {
        return $this->price;
    }

    public function setPrice(int $price): static
    {
        $this->price = $price;

        return $this;
    }

    public function getHorsepower(): ?int
    {
        return $this->horsepower;
    }

    public function setHorsepower(int $horsepower): static
    {
        $this->horsepower = $horsepower;

        return $this;
    }

    public function getTorque(): ?int
    {
        return $this->torque;
    }

    public function setTorque(int $torque): static
    {
        $this->torque = $torque;

        return $this;
    }

    public function getAcceleration(): ?string
    {
        return $this->acceleration;
    }

    public function setAcceleration(string $acceleration): static
    {
        $this->acceleration = $acceleration;

        return $this;
    }

    public function getEngineSize(): ?float
    {
        return $this->engineSize;
    }

    public function setEngineSize(float $engineSize): static
    {
        $this->engineSize = $engineSize;

        return $this;
    }

    public function getDrivetrain(): ?string
    {
        return $this->drivetrain;
    }

    public function setDrivetrain(string $drivetrain): static
    {
        $this->drivetrain = $drivetrain;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): static
    {
        $this->description = $description;

        return $this;
    }

    public function getAvailable(): ?bool
    {
        return $this->available;
    }

    public function setAvailable(bool $available): static
    {
        $this->available = $available;

        return $this;
    }

    public function getBrand(): string
    {
        return $this->brand;
    }

    public function setBrand(string $brand): void
    {
        $this->brand = $brand;
    }

    public function getModel(): string
    {
        return $this->model;
    }

    public function setModel(string $model): void
    {
        $this->model = $model;
    }

    public function getColor(): string
    {
        return $this->color;
    }

    public function setColor(string $color): void
    {
        $this->color = $color;
    }

    /**
     * @return Collection<int, MediaObject>
     */
    public function getImages(): Collection
    {
        return $this->images;
    }

    public function addImage(MediaObject $image): static
    {
        if (!$this->images->contains($image)) {
            $this->images[] = $image;
            $image->setCar($this);
        }

        return $this;
    }

    #[Groups(['car:read', 'car:item:read'])]
    public function getMainImage(): string
    {
        return $this->filePath;
//        return '/media/' . $this->filePath;
    }

    /**
     * @return Collection<int, Rent>
     */
    public function getRents(): Collection
    {
        return $this->rents;
    }

    public function addRent(Rent $rent): static
    {
        if (!$this->rents->contains($rent)) {
            $this->rents->add($rent);
            $rent->setCar($this);
        }

        return $this;
    }

    public function removeRent(Rent $rent): static
    {
        if ($this->rents->removeElement($rent)) {
            // set the owning side to null (unless already changed)
            if ($rent->getCar() === $this) {
                $rent->setCar(null);
            }
        }

        return $this;
    }
}
