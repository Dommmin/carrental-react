import {useAuth} from "../hooks/useAuth.js";
import {useState} from "react";
import {useNavigate} from "react-router-dom";
import Errors from "../components/Errors.jsx";
import axios from "../lib/axios";

function Create() {
    const navigate = useNavigate();

    const [brand, setBrand] = useState("");
    const [model, setModel] = useState("");
    const [year, setYear] = useState(2023);
    const [fuel, setFuel] = useState('');
    const [fuelConsumption, setFuelConsumption] = useState(0);
    const [seats, setSeats] = useState(5);
    const [transmission, setTransmission] = useState('');
    const [price, setPrice] = useState(0);
    const [horsepower, setHorsepower] = useState(0);
    const [torque, setTorque] = useState(0);
    const [acceleration, setAcceleration] = useState(0);
    const [engineSize, setEngineSize] = useState(1);
    const [drivetrain, setDrivetrain] = useState('');
    const [color, setColor] = useState("");
    const [description, setDescription] = useState("");
    const [mainPhoto, setMainPhoto] = useState(null);
    const [additionalImages, setAdditionalImages] = useState([]);
    const [errors, setErrors] = useState([]);
    const [isLoading, setIsLoading] = useState(false);

    // const { user } = useAuth({middleware: 'auth'});

    const currentYear = new Date().getFullYear()

    const handleRemoveImage = (indexToRemove) => {
        setAdditionalImages((prevImages) =>
            prevImages.filter((_, index) => index !== indexToRemove)
        );
    };

    const handleChangeImageOrder = (currentIndex, newIndex) => {
        setAdditionalImages((prevImages) => {
            const updatedImages = [...prevImages];
            const [movedImage] = updatedImages.splice(currentIndex, 1);
            updatedImages.splice(newIndex, 0, movedImage);
            return updatedImages;
        });
    };

    const handleMainPhotoChange = (event) => {
        setMainPhoto(event.target.files[0]);
    };

    const handleAdditionalImagesChange = (event) => {
        const files = event.target.files;
        setAdditionalImages([...additionalImages, ...Array.from(files)]);
    };

    async function handleFormSubmit(event) {
        event.preventDefault();

        if (!mainPhoto) {
            return alert('Please add a main photo');
        }

        if (additionalImages.length < 6) {
            return alert('Please add 6 additional images');
        }

        if (additionalImages.length > 6) {
            return alert('Please remove some additional images');
        }

        const formData = new FormData();

        // additionalImages.forEach((file, index) => {
        //     formData.append(`images[${index}]`, file);
        // });

        formData.append('file', mainPhoto);
        formData.append('brand', brand);
        formData.append('model', model);
        formData.append('year', year);
        formData.append('fuel', fuel);
        formData.append('fuelConsumption', fuelConsumption);
        formData.append('seats', seats);
        formData.append('transmission', transmission);
        formData.append('price', price);
        formData.append('horsepower', horsepower);
        formData.append('torque', torque);
        formData.append('acceleration', acceleration);
        formData.append('engineSize', engineSize);
        formData.append('drivetrain', drivetrain);
        formData.append('color', color);
        formData.append('description', description);

        try {
            setIsLoading(true);
            const response = await axios.post('/api/cars', formData, {
                headers: {
                    "Content-Type": "multipart/form-data",
                },
            });

            if (response.status === 201) {
                const data = new FormData();

                data.append('car_id', response.data.id);

                for (const file of additionalImages) {
                    data.append('file', file);

                    try {
                        await axios.post('/api/media_objects', data, {
                            headers: {
                                "Content-Type": "multipart/form-data",
                            },
                        });
                    } catch (error) {
                        console.log(error);
                    }
                }

                navigate('/cars');
            }
        } catch (error) {
            console.log(error);
        }

        setIsLoading(false);
    }

    return (
        <div className="flex items-center justify-center p-12 w-full">
            <div className="mx-auto w-full bg-white dark:bg-gray-700 text-gray-800 dark:text-white">
                <form className="py-6 px-9" onSubmit={handleFormSubmit}>
                    <div className="mb-5">
                        <label
                            htmlFor="brand"
                            className="mb-3 block text-base font-medium dark:text-white text-[#07074D]"
                        >
                            Brand
                        </label>
                        <input
                            value={brand}
                            onChange={(event) => setBrand(event.target.value)}
                            type="text"
                            name="brand"
                            id="brand"
                            className="w-full rounded-md border border-[#e0e0e0] bg-white dark:bg-gray-600 py-3 px-6 text-base font-medium text-[#6B7280] dark:text-white outline-none focus:border-[#6A64F1] focus:shadow-md"
                        />
                        <Errors errors={errors} field="brand"/>
                    </div>
                    <div className="mb-5">
                        <label
                            htmlFor="model"
                            className="mb-3 block text-base font-medium dark:text-white text-[#07074D]"
                        >
                            Model
                        </label>
                        <input
                            value={model}
                            onChange={(event) => setModel(event.target.value)}
                            type="text"
                            name="model"
                            id="model"
                            className="w-full rounded-md border border-[#e0e0e0] bg-white dark:bg-gray-600 py-3 px-6 text-base font-medium text-[#6B7280] dark:text-white outline-none focus:border-[#6A64F1] focus:shadow-md"
                        />
                        <Errors errors={errors} field="model"/>
                    </div>
                    <div className="mb-5">
                        <label
                            htmlFor="color"
                            className="mb-3 block text-base font-medium dark:text-white text-[#07074D]"
                        >
                            Color
                        </label>
                        <input
                            value={color}
                            onChange={(event) => setColor(event.target.value)}
                            type="text"
                            name="color"
                            id="color"
                            className="w-full rounded-md border border-[#e0e0e0] bg-white dark:bg-gray-600 py-3 px-6 text-base font-medium text-[#6B7280] dark:text-white outline-none focus:border-[#6A64F1] focus:shadow-md"
                        />
                        <Errors errors={errors} field="color"/>
                    </div>
                    <div className="mb-5">
                        <label
                            htmlFor="year"
                            className="mb-3 block text-base font-medium dark:text-white text-[#07074D]"
                        >
                            Year
                        </label>
                        <input
                            value={year}
                            onChange={(event) => setYear(Number(event.target.value))}
                            type="number"
                            name="name"
                            id="name"
                            min={currentYear - 5}
                            max={currentYear}
                            className="w-full rounded-md border border-[#e0e0e0] bg-white dark:bg-gray-600 py-3 px-6 text-base font-medium text-[#6B7280] dark:text-white outline-none focus:border-[#6A64F1] focus:shadow-md"
                        />
                        <Errors errors={errors} field="year"/>
                    </div>

                    <div className="mb-5">
                        <label
                            htmlFor="fuel"
                            className="mb-3 block text-base font-medium dark:text-white text-[#07074D]"
                        >
                            Fuel
                        </label>
                        <select
                            onChange={event => setFuel(event.target.value)}
                            value={fuel}
                            name="fuel"
                            id="fuel"
                            className="w-full rounded-md border border-[#e0e0e0] bg-white dark:bg-gray-600 py-3 px-6 text-base font-medium text-[#6B7280] dark:text-white outline-none focus:border-[#6A64F1] focus:shadow-md"
                        >
                            <option value="Petrol">Benzyna</option>
                            <option value="Diesel">Diesel</option>
                            <option value="Electric">Elektryczny</option>
                        </select>
                        <Errors errors={errors} field="fuel"/>
                    </div>

                    <div className="mb-5">
                        <label
                            htmlFor="fuel_consumption"
                            className="mb-3 block text-base font-medium dark:text-white text-[#07074D]"
                        >
                            Fuel Consumption
                        </label>
                        <input
                            value={fuelConsumption}
                            onChange={(event) => setFuelConsumption(Number(event.target.value))}
                            type="number"
                            name="fuel_consumption"
                            id="fuel_consumption"
                            min="0"
                            step="0.1"
                            className="w-full rounded-md border border-[#e0e0e0] bg-white dark:bg-gray-600 py-3 px-6 text-base font-medium text-[#6B7280] dark:text-white outline-none focus:border-[#6A64F1] focus:shadow-md"
                        />
                        <Errors errors={errors} field="fuel_consumption"/>
                    </div>

                    <div className="mb-5">
                        <label
                            htmlFor="seats"
                            className="mb-3 block text-base font-medium dark:text-white text-[#07074D]"
                        >
                            Seats
                        </label>
                        <input
                            value={seats}
                            onChange={(event) => setSeats(Number(event.target.value))}
                            type="number"
                            name="seats"
                            id="seats"
                            min="2"
                            max="9"
                            className="w-full rounded-md border border-[#e0e0e0] bg-white dark:bg-gray-600 py-3 px-6 text-base font-medium text-[#6B7280] dark:text-white outline-none focus:border-[#6A64F1] focus:shadow-md"
                        />
                        <Errors errors={errors} field="seats"/>
                    </div>

                    <div className="mb-5">
                        <label
                            htmlFor="transmission"
                            className="mb-3 block text-base font-medium dark:text-white text-[#07074D]"
                        >
                            Transmission
                        </label>
                        <select
                            onChange={event => setTransmission(event.target.value)}
                            value={transmission}
                            name="transmission"
                            id="transmission"
                            className="w-full rounded-md border border-[#e0e0e0] bg-white dark:bg-gray-600 py-3 px-6 text-base font-medium text-[#6B7280] dark:text-white outline-none focus:border-[#6A64F1] focus:shadow-md"
                        >
                            <option value="manual">Manualna</option>
                            <option value="automatic">Automatyczna</option>
                        </select>
                        <Errors errors={errors} field="transmission"/>
                    </div>
                    <div className="mb-5">
                        <label
                            htmlFor="horsepower"
                            className="mb-3 block text-base font-medium dark:text-white text-[#07074D]"
                        >
                            Horsepower (HP)
                        </label>
                        <input
                            value={horsepower}
                            onChange={(event) => setHorsepower(Number(event.target.value))}
                            type="number"
                            name="horsepower"
                            id="horsepower"
                            min="0"
                            className="w-full rounded-md border border-[#e0e0e0] bg-white dark:bg-gray-600 py-3 px-6 text-base font-medium text-[#6B7280] dark:text-white outline-none focus:border-[#6A64F1] focus:shadow-md"
                        />
                        <Errors errors={errors} field="horsepower"/>
                    </div>
                    <div className="mb-5">
                        <label
                            htmlFor="torque"
                            className="mb-3 block text-base font-medium dark:text-white text-[#07074D]"
                        >
                            Torque (Nm)
                        </label>
                        <input
                            value={torque}
                            onChange={(event) => setTorque(Number(event.target.value))}
                            type="number"
                            name="torque"
                            id="torque"
                            min="0"
                            className="w-full rounded-md border border-[#e0e0e0] bg-white dark:bg-gray-600 py-3 px-6 text-base font-medium text-[#6B7280] dark:text-white outline-none focus:border-[#6A64F1] focus:shadow-md"
                        />
                        <Errors errors={errors} field="torque"/>
                    </div>
                    <div className="mb-5">
                        <label
                            htmlFor="acceleration"
                            className="mb-3 block text-base font-medium dark:text-white text-[#07074D]"
                        >
                            Acceleration ({acceleration}s - 100 km/h)
                        </label>
                        <input
                            value={acceleration}
                            onChange={(event) => setAcceleration(Number(event.target.value))}
                            type="number"
                            name="acceleration"
                            id="acceleration"
                            min="0"
                            step="0.1"
                            className="w-full rounded-md border border-[#e0e0e0] bg-white dark:bg-gray-600 py-3 px-6 text-base font-medium text-[#6B7280] dark:text-white outline-none focus:border-[#6A64F1] focus:shadow-md"
                        />
                        <Errors errors={errors} field="acceleration"/>
                    </div>
                    <div className="mb-5">
                        <label
                            htmlFor="engineSize"
                            className="mb-3 block text-base font-medium dark:text-white text-[#07074D]"
                        >
                            Engine Size (liters)
                        </label>
                        <input
                            value={engineSize}
                            onChange={(event) => setEngineSize(Number(event.target.value))}
                            type="number"
                            name="engineSize"
                            id="engineSize"
                            min="1"
                            step="0.1"
                            className="w-full rounded-md border border-[#e0e0e0] bg-white dark:bg-gray-600 py-3 px-6 text-base font-medium text-[#6B7280] dark:text-white outline-none focus:border-[#6A64F1] focus:shadow-md"
                        />
                        <Errors errors={errors} field="engineSize"/>
                    </div>
                    <div className="mb-5">
                        <label
                            htmlFor="drivetrain"
                            className="mb-3 block text-base font-medium dark:text-white text-[#07074D]"
                        >
                            Drivetrain
                        </label>
                        <select
                            onChange={event => setDrivetrain(event.target.value)}
                            value={drivetrain}
                            name="drivetrain"
                            id="drivetrain"
                            className="w-full rounded-md border border-[#e0e0e0] bg-white dark:bg-gray-600 py-3 px-6 text-base font-medium text-[#6B7280] dark:text-white outline-none focus:border-[#6A64F1] focus:shadow-md"
                        >
                            <option value="fwd">Front Wheel Drive</option>
                            <option value="rwd">Rear Wheel Drive</option>
                            <option value="awd">All Wheel Drive</option>
                        </select>
                        <Errors errors={errors} field="drivetrain"/>
                    </div>
                    <div className="mb-5">
                        <label
                            htmlFor="price"
                            className="mb-3 block text-base font-medium dark:text-white text-[#07074D]"
                        >
                            Price (${price} / day)
                        </label>
                        <input
                            value={price}
                            onChange={(event) => setPrice(Number(event.target.value))}
                            type="number"
                            name="price"
                            id="price"
                            min="0"
                            step="50"
                            className="w-full rounded-md border border-[#e0e0e0] bg-white dark:bg-gray-600 py-3 px-6 text-base font-medium text-[#6B7280] dark:text-white outline-none focus:border-[#6A64F1] focus:shadow-md"
                        />
                        <Errors errors={errors} field="price"/>
                    </div>
                    <div className="mb-5">
                        <label
                            htmlFor="description"
                            className="mb-3 block text-base font-medium dark:text-white text-[#07074D]"
                        >
                            Description
                        </label>
                        <textarea
                            rows="10"
                            value={description}
                            onChange={(event) => setDescription(event.target.value)}
                            name="description"
                            id="description"
                            className="w-full rounded-md border border-[#e0e0e0] bg-white dark:bg-gray-600 py-3 px-6 text-base font-medium text-[#6B7280] dark:text-white outline-none focus:border-[#6A64F1] focus:shadow-md"
                        />
                        <Errors errors={errors} field="content"/>
                    </div>
                    <div className="text-center mt-4 mb-4 flex justify-center">
                        <div>
                            <input
                                type="file"
                                name="mainPhoto"
                                id="mainPhoto"
                                onChange={handleMainPhotoChange}
                                className="hidden"
                            />
                            <label
                                htmlFor="mainPhoto"
                                className="cursor-pointer bg-emerald-600 text-white py-2 px-4 rounded-md hover:bg-emerald-700"
                            >
                                Choose Main Photo
                            </label>

                            {mainPhoto && (
                                <div className="mt-4 flex justify-center">
                                    <img
                                        src={URL.createObjectURL(mainPhoto)}
                                        alt="Selected Main"
                                        className="mt-2 rounded-md"
                                        style={{maxWidth: "100%", maxHeight: "200px"}}
                                    />
                                </div>
                            )}
                        </div>
                    </div>
                    <div className="border-b-2"></div>

                    <div className="mt-4 mb-4 text-center">
                        <input
                            type="file"
                            name="additionalImages"
                            id="additionalImages"
                            onChange={handleAdditionalImagesChange}
                            className="hidden"
                            multiple
                        />
                        <label
                            htmlFor="additionalImages"
                            className="cursor-pointer bg-violet-600 text-white py-2 px-4 rounded-md hover:bg-violet-700"
                        >
                            Choose Additional Images
                        </label>

                        {additionalImages.length > 0 && (
                            <div className="mt-4 mb-4 grid grid-cols-2 md:grid-cols-3 lg:grid-cols-4 gap-4 border rounded-lg p-4">
                                {additionalImages.map((file, index) => (
                                    <div key={index} className="relative">
                                        <img
                                            src={URL.createObjectURL(file)}
                                            alt={`Selected ${index + 1}`}
                                            className="rounded-md"
                                            style={{maxWidth: "100%", maxHeight: "200px"}}
                                        />
                                        <div className="absolute top-2 right-2 flex space-x-2">
                                            <span
                                                className="cursor-pointer text-red-500 hover:text-red-700"
                                                onClick={() => handleRemoveImage(index)}
                                            >
                                              <svg
                                                  xmlns="http://www.w3.org/2000/svg"
                                                  fill="none"
                                                  viewBox="0 0 24 24"
                                                  stroke="currentColor"
                                                  className="h-5 w-5"
                                              >
                                                <path
                                                    strokeLinecap="round"
                                                    strokeLinejoin="round"
                                                    strokeWidth="2"
                                                    d="M6 18L18 6M6 6l12 12"
                                                />
                                              </svg>
                                            </span>
                                        </div>
                                        <div className="flex justify-between mt-2">
                                            <button
                                                disabled={index <= 0}
                                                className={`cursor-pointer text-blue-500 hover:text-blue-700 bg-gray-800 rounded-lg py-2 ${index <= 0 ? "opacity-50" : ""}`}
                                                onClick={(event) => {
                                                    event.preventDefault();
                                                    handleChangeImageOrder(index, index - 1)
                                                }}
                                            >
                                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                                     strokeWidth="1.5" stroke="currentColor" data-slot="icon"
                                                     className="w-6 h-6">
                                                  <path strokeLinecap="round" strokeLinejoin="round"
                                                        d="M15.75 19.5 8.25 12l7.5-7.5"/>
                                                </svg>
                                            </button>
                                                <button
                                                    disabled={index > additionalImages.length - 1}
                                                    className={`cursor-pointer text-blue-500 hover:text-blue-700 bg-gray-800 rounded-lg py-2 ${index >= additionalImages.length - 1 ? "opacity-50" : ""}`}
                                                    onClick={(event) => {
                                                        event.preventDefault();
                                                        handleChangeImageOrder(index, index + 1)
                                                    }}
                                                >
                                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                                     strokeWidth="1.5" stroke="currentColor" data-slot="icon"
                                                     className="w-6 h-6">
                                                  <path strokeLinecap="round" strokeLinejoin="round"
                                                        d="m8.25 4.5 7.5 7.5-7.5 7.5"/>
                                                </svg>
                                              </button>
                                        </div>
                                    </div>
                                ))}
                            </div>

                        )}
                    </div>
                    <Errors errors={errors} field="file"/>

                    <div>
                        <button
                            disabled={isLoading}
                            className={`hover:shadow-form w-full rounded-md bg-cyan-600 hover:bg-cyan-700 py-3 px-8 text-center text-base font-semibold text-white outline-none ${isLoading ? "opacity-50" : ""}`}
                            type="submit"
                        >
                            Save
                        </button>
                    </div>
                </form>
            </div>
        </div>
    );
}

export default Create;
