import {useNavigate, useParams} from "react-router-dom";
import {useEffect, useRef, useState} from "react";
import axios from "../lib/axios.js";
import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import Errors from "../components/Errors.jsx";
import CustomDatePicker from "../components/CustomDatePicker.jsx";

function Show() {
    const navigate = useNavigate();
    const { slug } = useParams();
    const [car, setCar] = useState({});
    const [isLoading, setIsLoading] = useState(true);
    const [isModalOpen, setIsModalOpen] = useState(false);
    const [totalPrice, setTotalPrice] = useState(0);
    const [networkError, setNetworkError] = useState(false);
    const [startDate, setStartDate] = useState(null);
    const [endDate, setEndDate] = useState(null);
    const [duration, setDuration] = useState(0);
    const modalRef = useRef(null);

    useEffect(() => {
        const handleModalClick = (event) => {
            if (modalRef.current && !modalRef.current.contains(event.target)) {
                closeModal();
            }
        };

        if (isModalOpen) {
            document.addEventListener('mousedown', handleModalClick);
        } else {
            document.removeEventListener('mousedown', handleModalClick);
        }

        return () => {
            document.removeEventListener('mousedown', handleModalClick);
        };
    }, [isModalOpen]);

    const openModal = () => {
        // if (!user) {
        //     navigate('/login');
        // }

        setIsModalOpen(true);
    };

    const closeModal = () => {
        setIsModalOpen(false);
    };

    const fetchCar = async () => {
        try {
            const response = await axios.get(`/api/cars/${slug}`);
            setCar(response.data);
        } catch (error) {
            if (error.code === 'ERR_NETWORK') {
                setNetworkError(true);
            }

            if (error.response?.status === 500) {
                setNetworkError(true);
            }
        }

        setIsLoading(false);
    };

    useEffect(() => {
        fetchCar();
    }, [slug]);

    if (isLoading) {
        return <div className="flex justify-center items-center h-screen"><div className="loading loading-spinner loading-lg"></div></div>;
    }

    if (networkError) {
        return <div className="flex justify-center items-center h-screen">Please try again later or contact support.</div>;
    }

    if (Object.keys(car).length === 0) {
        return <div className="flex justify-center items-center h-screen">Car not found.</div>;
    }

    async function handleRent() {
        if (!startDate || !endDate) {
            return;
        }

        try {
            await axios.post('/api/rents', {
                car: '/api/cars/' + car.slug,
                user: '/api/users/260',
                startDate: startDate,
                endDate: endDate,
            })
        } catch (error) {
            console.log(error);
        }

        console.log('rent');
    }

    return (
        <div className="container mx-auto mt-8">
            <div className="flex flex-wrap justify-center">
                <img
                    src={`http://localhost:8000/${car.mainImage}`}
                    alt={car.name}
                    className="rounded-lg w-full md:w-96 h-auto mb-4"
                />

                <div className="flex flex-col mt-4 mx-4">
                    <h1 className="text-3xl font-bold mb-2">{car.name}</h1>
                    <div className="text-lg mb-2">Year: {car.year}</div>
                    <div className="text-lg mb-2">Fuel: {car.fuel}</div>
                    <div className="text-lg mb-2">Fuel Consumption: {car.fuelConsumption}l / 100km</div>
                    <div className="text-lg mb-2">Seats: {car.seats}</div>
                    <div className="text-lg mb-2">Transmission: {car.transmission}</div>
                    <div className="text-lg mb-2">Horsepower: {car.horsepower} HP</div>
                    <div className="text-lg mb-2">Torque: {car.torque} Nm</div>
                    <div className="text-lg mb-2">Acceleration: {car.acceleration}s - 100km/h</div>
                    <div className="text-lg mb-2">Drivetrain: {car.drivetrain}</div>
                    <div className={`text-lg ${car.available ? 'text-green-500' : 'text-red-500'} mb-2`}>
                        {car.available ? 'Available' : 'Not Available'}
                    </div>
                </div>
            </div>

            <p className="text-center text-base mt-4">{car.description}</p>
            <Slider
                className="mt-8"
                dots
                infinite
                speed={500}
                slidesToShow={3}
                slidesToScroll={1}
            >
                {car.images.map((image) => (
                    <img
                        src={`http://localhost:8000/${image.path}`}
                        alt=""
                        className="rounded-lg w-32 md:w-48 h-auto"  // Adjust the width
                        key={image.id}
                    />
                ))}
            </Slider>
            <div className="flex justify-center items-center mt-8">
                <button onClick={openModal} className="btn btn-outline btn-success">
                    Rent Car
                </button>
            </div>
            <div>
                {isModalOpen && (
                    <div className="fixed top-0 left-0 w-full h-full flex items-center justify-center">
                        <div ref={modalRef}
                             className="bg-gray-100 dark:bg-gray-700 p-8 rounded-md w-full max-w-3xl h-[450px] flex flex-col">
                            <div className="text-center text-2xl font-bold mb-4">
                                Rent {car.name}
                            </div>
                            <CustomDatePicker
                                rents={car.rents}
                                price={car.price}
                                setTotalPrice={setTotalPrice}
                                setStartDate={setStartDate}
                                setEndDate={setEndDate}
                                startDate={startDate}
                                endDate={endDate}
                                setDuration={setDuration}
                            />
                            {!duration
                                ? <div className="flex justify-center items-center mt-8 text-3xl font-bold text-error">Select Rental
                                    Period</div>
                                : <>
                                    <div className="flex justify-between items-center mt-8 text-3xl font-bold">
                                        <div>Duration:</div>
                                        <div>{duration === 0 || duration > 1 ? duration + ' days' : duration + ' day'}</div>
                                    </div>
                                    <div className="flex justify-between items-center text-3xl font-bold">
                                        <div>Total Price:</div>
                                        <div>${totalPrice}</div>
                                    </div>
                                </>
                            }

                            <div className="flex-1"/>
                            <div className="flex justify-center items-end mt-8">
                                <button
                                    disabled={!startDate || !endDate}
                                    onClick={handleRent}
                                    className="btn btn-outline btn-succes w-full"
                                >Rent now!
                                </button>
                            </div>
                        </div>
                    </div>

                )}
            </div>
        </div>
    );
}

export default Show;
