import { useEffect, useState } from "react";
import axios from "../lib/axios";
import Car from "../components/Car.jsx";

function Index() {
    const [cars, setCars] = useState([]);
    const [isLoading, setIsLoading] = useState(true);

    const fetchCars = async () => {
        try {
            const response = await axios.get('/api/cars');
            setCars(response.data);
        } catch (error) {
            console.log(error);
        }

        setIsLoading(false);
    };

    useEffect(() => {
        fetchCars();
    }, []);

    if (isLoading) {
        return (
            <div className="flex justify-center items-center h-screen">
                <div className="loading loading-spinner loading-lg"></div>
            </div>
        );
    }

    return (
        <div className="container mx-auto mt-8">
            <h1 className="text-3xl font-bold mb-4">Car List</h1>

            {cars.length > 0 ? (
                <ul className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-4">
                    {cars.map((car) => (
                        <Car key={car.id} {...car} />
                    ))}
                </ul>
            ) : (
                <p className="text-gray-600">No cars available.</p>
            )}
        </div>
    );
}

export default Index;
