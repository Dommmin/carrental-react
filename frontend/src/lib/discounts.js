const discounts =  {
    3: 0.1,
    7: 0.2,
    14: 0.3,
    30: 0.4,
}

export default discounts;