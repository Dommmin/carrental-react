import Axios from 'axios'

const axios = Axios.create({
    baseURL: 'http://localhost:8000',
    headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
    },
})

if (localStorage.getItem('access_token') && localStorage.getItem('access_token') !== 'undefined') {
    axios.defaults.headers['Authorization'] = 'Bearer ' + localStorage.getItem('access_token')
}

export default axios