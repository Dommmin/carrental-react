import React, {useEffect, useMemo} from 'react';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import {eachDayOfInterval, isSameDay} from "date-fns";
import discounts from "../lib/discounts.js";

const CustomDatePicker = ({ rents, setTotalPrice, price, startDate, endDate, setStartDate, setEndDate, setDuration }) => {
    const filterStartTime = (time) => {
        const currentTime = new Date().getTime();
        const selectedTime = new Date(time).getTime();

        if (currentTime >= selectedTime) {
            return false; // Selected time is in the past, so it's not available
        }

        if (endDate && selectedTime >= endDate.getTime()) {
            return false; // Selected time is after the end date, so it's not available
        }

        for (const rent of rents) {
            if (selectedTime >= rent.startDate * 1000 && selectedTime <= rent.endDate * 1000) {
                return false; // Selected time is inside a rent range, so it's not available
            }
        }

        return true;
    }

    const filterEndTime = (time) => {
        const currentTime = new Date().getTime() + 60 * 60 * 1000;
        const selectedTime = new Date(time).getTime();

        if (currentTime >= selectedTime) {
            return false; // Selected time is in the past, so it's not available
        }

        if (startDate && selectedTime <= startDate.getTime()) {
            return false; // Selected time is before the start date, so it's not available
        }

        for (const rent of rents) {
            if (selectedTime >= rent.startDate * 1000 && selectedTime <= rent.endDate * 1000) {
                return false; // Selected time is inside a rent range, so it's not available
            }
        }

        return true;
    };

    const unavailableDays = useMemo(() => {
        const daysMap = new Map();

        rents.forEach(rent => {
            const startOfDay = rent.startDate * 1000;
            const endOfDay = rent.endDate * 1000;

            const daysInRentRange = eachDayOfInterval({ start: startOfDay, end: endOfDay });

            daysInRentRange.forEach(day => {
                const dayString = day.toISOString().split('T')[0];

                if (!daysMap.has(dayString)) {
                    daysMap.set(dayString, []);
                }

                daysMap.get(dayString).push({ start: startOfDay, end: endOfDay });
            });
        });

        const unavailableDaysArray = [];
        daysMap.forEach((hours, dayString) => {
            const allHoursUnavailable = Array.from({ length: 24 }, (_, index) => {
                const hourStartTime = new Date(dayString).getTime() + index * 60 * 60 * 1000;
                const hourEndTime = hourStartTime + 60 * 60 * 1000;

                return hours.some(rent => hourStartTime >= rent.start && hourEndTime <= rent.end);
            }).every(unavailable => unavailable);

            if (allHoursUnavailable) {
                unavailableDaysArray.push(new Date(dayString));
            }
        });

        return unavailableDaysArray;
    }, [rents]);

    const filterDate = (date) => {
        return !unavailableDays.some(day => isSameDay(day, date));
    };

    useEffect(() => {
        if (!startDate || !endDate) {
            return;
        }

        const days = Math.ceil((endDate - startDate) / (1000 * 60 * 60 * 24));

        if (days <= 0) {
            setTotalPrice(0);
            return;
        }

        setDuration(days);

        // Apply discounts
        const discountPercentage = Object.keys(discounts)
            .sort((a, b) => b - a)
            .find(discountDays => days >= parseInt(discountDays, 10));

        const discount = discountPercentage ? discounts[discountPercentage] : 0;

        const discountedPrice = price - price * discount;
        const totalPrice = discountedPrice * days;

        setTotalPrice(Math.round(totalPrice));
    }, [startDate, endDate, price]);

    return (
        <div className="flex justify-center space-x-4 mt-4">
            <div className="flex flex-col items-center">
                <label className="block text-sm font-medium text-gray-700">
                    Start Date
                </label>
                <DatePicker
                    showIcon
                    selected={startDate}
                    onChange={(date) => setStartDate(date)}
                    showTimeSelect
                    timeFormat="HH:mm"
                    timeIntervals={60}
                    timeCaption="Time"
                    dateFormat="MMMM d, yyyy h:mm aa"
                    filterDate={filterDate}
                    filterTime={filterStartTime}
                    minDate={new Date()}
                    maxDate={endDate}
                    className="border rounded-md"
                    icon={
                        <svg
                            xmlns="http://www.w3.org/2000/svg"
                            width="1em"
                            height="1em"
                            viewBox="0 0 48 48"
                        >
                            <mask id="ipSApplication0">
                                <g fill="none" stroke="#fff" strokeLinejoin="round" strokeWidth="4">
                                    <path strokeLinecap="round" d="M40.04 22v20h-32V22"></path>
                                    <path
                                        fill="#fff"
                                        d="M5.842 13.777C4.312 17.737 7.263 22 11.51 22c3.314 0 6.019-2.686 6.019-6a6 6 0 0 0 6 6h1.018a6 6 0 0 0 6-6c0 3.314 2.706 6 6.02 6c4.248 0 7.201-4.265 5.67-8.228L39.234 6H8.845l-3.003 7.777Z"
                                    ></path>
                                </g>
                            </mask>
                            <path
                                fill="currentColor"
                                d="M0 0h48v48H0z"
                                mask="url(#ipSApplication0)"
                            ></path>
                        </svg>
                    }
                />
            </div>
            <div className="flex flex-col items-center">
                <label className="block text-sm font-medium text-gray-700">
                    End Date
                </label>
                <DatePicker
                    showIcon
                    disabled={startDate === null}
                    selected={endDate}
                    onChange={(date) => setEndDate(date)}
                    showTimeSelect
                    timeFormat="HH:mm"
                    timeIntervals={60}
                    timeCaption="Time"
                    dateFormat="MMMM d, yyyy h:mm aa"
                    filterDate={filterDate}
                    filterTime={filterEndTime}
                    minDate={startDate}
                    className="border rounded-md"
                    icon={
                        <svg
                            xmlns="http://www.w3.org/2000/svg"
                            width="1em"
                            height="1em"
                            viewBox="0 0 48 48"
                        >
                            <mask id="ipSApplication0">
                                <g fill="none" stroke="#fff" strokeLinejoin="round" strokeWidth="4">
                                    <path strokeLinecap="round" d="M40.04 22v20h-32V22"></path>
                                    <path
                                        fill="#fff"
                                        d="M5.842 13.777C4.312 17.737 7.263 22 11.51 22c3.314 0 6.019-2.686 6.019-6a6 6 0 0 0 6 6h1.018a6 6 0 0 0 6-6c0 3.314 2.706 6 6.02 6c4.248 0 7.201-4.265 5.67-8.228L39.234 6H8.845l-3.003 7.777Z"
                                    ></path>
                                </g>
                            </mask>
                            <path
                                fill="currentColor"
                                d="M0 0h48v48H0z"
                                mask="url(#ipSApplication0)"
                            ></path>
                        </svg>
                    }
                />
            </div>
        </div>
    );
}

export default CustomDatePicker;
