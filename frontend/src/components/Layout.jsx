import { Link, Outlet } from "react-router-dom";
import {useAuth} from "../hooks/useAuth.js";

function Layout() {
    const {logout, user} = useAuth({
        middleware: 'guest',
    });

    return (
        <div className="bg-gray-100 dark:bg-gray-900">
            <div className="container mx-auto p-4">
                <div className="flex justify-between items-center py-4">
                    <Link to="/" className="text-xl font-bold text-purple-600 dark:text-purple-300">
                        HomePage
                    </Link>
                    <nav className="space-x-4">
                        <Link to="/cars" className="text-gray-600 dark:text-gray-300 hover:text-gray-800 dark:hover:text-gray-100">
                            Cars
                        </Link>
                        {/*{user &&*/}
                            <Link to="/cars/create" className="text-gray-600 dark:text-gray-300 hover:text-gray-800 dark:hover:text-gray-100">
                                Create Car
                            </Link>
                        {/*}*/}

                        <span>{user?.username}</span>
                        {!user
                            ?
                            <>
                                <Link to="/login"
                                      className="text-gray-600 dark:text-gray-300 hover:text-gray-800 dark:hover:text-gray-100">
                                    <button className="btn btn-outline btn-accent">Login</button>
                                </Link>
                                <Link to="/register"
                                      className="text-gray-600 dark:text-gray-300 hover:text-gray-800 dark:hover:text-gray-100">
                                    <button className="btn btn-outline btn-primary">Register</button>
                                </Link>
                            </>
                            :
                            <button onClick={logout} className="btn btn-outline btn-error">Logout</button>
                        }
                    </nav>
                </div>
                <div className="flex flex-col justify-center items-center min-h-screen">
                    <Outlet />
                </div>
            </div>
        </div>
    );
}

export default Layout;
