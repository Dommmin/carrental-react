import React from "react";
import { Link } from "react-router-dom";

const Car = ({ id, slug, name, year, fuel, fuelConsumption, seats, transmission, mainImage, available }) => {
    return (
        <li
            key={id}
            className={`text-white p-4 rounded-md shadow-md transition-transform transform hover:scale-105 duration-300 ease-in-out ${
                available ? "bg-green-600" : "bg-red-600"
            }`}
        >
            <Link to={`/cars/${slug}`} className="block">
                <img
                    src={mainImage}
                    // src={`http://localhost:8000/${mainImage}`}
                    alt={name}
                    className="rounded-md w-full h-40 object-cover mb-4 transition-opacity opacity-80 hover:opacity-100 duration-300 ease-in-out"
                />
                <div className="flex justify-between items-center">
                    <div>
                        <h2 className="text-xl font-semibold mb-2">{name}</h2>
                        <p>{year}</p>
                    </div>
                </div>
                <div className="mt-2">
                    <p>{fuel}</p>
                    <p>Fuel Consumption: {fuelConsumption}l / 100km</p>
                    <p>Seats: {seats}</p>
                    <p>Transmission: {transmission}</p>
                </div>
            </Link>
        </li>
    );
};

export default Car;