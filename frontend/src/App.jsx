import {createBrowserRouter, RouterProvider} from "react-router-dom";
import CarsIndex from './Cars/Index'
import CarsShow from './Cars/Show'
import CarsCreate from './Cars/Create'
import Layout from "./components/Layout.jsx";

function App() {
    const router = createBrowserRouter([
        {
            path: "/",
            element: <Layout />,
            children: [
                {
                    path: "/cars",
                    element: <CarsIndex />
                },
                {
                    path: "/cars/create",
                    element: <CarsCreate />
                },
                {
                    path: "/cars/:slug",
                    element: <CarsShow />
                }
            ]
        },
        {
            path: "/login",
            element: <div className="min-h-screen flex items-center justify-center">There will be a Login form</div>
        },
        {
            path: "/register",
            element: <div className="min-h-screen flex items-center justify-center">There will be a registration form</div>
        }
    ])


    return (
        <RouterProvider router={router} />
  )
}

export default App
